﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title> Costa Rica </title>
    <link type="image/png" rel="icon" href="img/icon-costarica.jpg"/>
    <script type="text/javascript" src="lib/jquery.min.js"></script>
    <script type="text/javascript" src="lib/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="menu-shadow"></div>
<div class="menu-shadow-Register"></div>
<ul class="tab-background">
    <li>
        <a class="active" href="index.html"> <span class="glyphicon glyphicon-home"> </span> Home</a>
    </li>
    <li id="glyphicon-menu-position">
        <span class="glyphicon glyphicon-user" id="glyphicon-menu"> login </span>
    </li>
    <div class="menu">
        <h2 id="form-h2"> Login</h2>

        <form method="post" id="loginForm" action="php/login.php">
            <span class="glyphicon glyphicon-asterisk" id="glyphicon-name"></span><input type="text"
                                                                                         class="login-fields"
                                                                                         name="username"
                                                                                         placeholder="Your name">
            <span class="glyphicon glyphicon-asterisk" id="glyphicon-password"> </span> <input type="password"
                                                                                               name="password"
                                                                                               class="login-fields"
                                                                                               placeholder="Your password">
            <div>
                <input type="submit" id="sub" class="btn-Login" value="Login"> </input>
                <br>
                <a class="Registration">Sign up</a>
            </div>
        </form>
        <span id="result"> </span>
    </div>
</ul>
<div class="top">
    <h1> Costa Rica</h1>
</div>
<div class="big-image"></div>
<div class="center">
    <h2>About Costa Rica</h2>
    <hr/>
    <p class="center-P">
        De Republiek Costa Rica is een land in Centraal-Amerika dat in het noorden door Nicaragua en in het zuiden door
        Panama wordt begrensd. In het westen en het oosten wordt het respectievelijk begrensd door de Grote Oceaan en de
        Caraïbische Zee. Costa Rica, wat letterlijk Rijke kust betekent, ontbond zijn leger in 1949.[4] Costa Rica stond
        in 2010 als 62ste gerangschikt in de Index van menselijke ontwikkeling en is op dat vlak een van de toplanden in
        Latijns-Amerika.
        In 2007 kondigde de Costa Ricaanse regering aan dat het tegen 2021 het eerste klimaatneutrale land van de wereld
        zal zijn. Costa Rica stond in 2009 en 2012 dan ook als eerste gerangschikt in de Happy Planet Index, die
        aangeeft hoe gelukkig de mensen zijn op basis van onder andere de ecologische voetafdruk.
    </p>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4028022.0201549134!2d-86.4996864343992!3d9.628357536272183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f92e56221acc925%3A0x6254f72535819a2b!2sCosta+Rica!5e0!3m2!1snl!2snl!4v1454970836813"
            class="map"></iframe>
    <?php
    require_once ('php/DB.php');
    $mysqli = connectDB();
    $result = $mysqli->query("SELECT username,password FROM users");
    ?>
    <table class="table">
        <tr>
            <th>username</th>
            <th>password</th>
        </tr>
        <?php
        while($row = $result->fetch_assoc()){
            echo "<tr>";
            echo '<td>' . $row['username'] . '</td>';
            echo '<td>' . $row['password'] . '</td>';
            echo "</tr>";
        } ?>
    </table>
    <span>Search</span>
    <input type="text" name="search_text" placeholder="search by username" id="search_text"> </input>
    <div id="result">hier </div>
</div>
<script>
    $(document).ready(function() {
        $('#search_text').keyup(function () {
            var txt = $(this).val();
            if(txt!=''){

            }
            else {
                $('#result').html('');
                $.ajax({
                    url:'login.php',
                    method:"post",
                    data:{search:txt},
                    dataType:"text",
                    succes: function (data) {
                        $('#result').html(data);
                    }

                });
            }
        });
    });
</script>

<div class="left">
    <h2>Information</h2>
    <hr/>
    <ul class="left-links">
        <li>
            <a href="#">wiki Costa Rica</a>
        </li>

    </ul>
</div>

<form id="Registration-form">
    <div class="Registration-form-h2-background"><h2 id="Registration-form-h2">Register</h2></div>
    <input type="text" placeholder="Full name"> <br>
    <input type="text" placeholder="Username"> <br>
    <input type="text" placeholder="Password"> <br>
    <input type="text" placeholder="Re-type Password"> <br>
    <input type="text" placeholder="Age"><br>
    <input type="text" placeholder="Gender"> <br>
    <input type="text" placeholder="Birthplace"> <br>
    <button type="submit">Register</button>
</form>

<div class="footer">
    <p class="footer-P">
        Copyright Diego Roa Centeno
    </p>
</div>

<!-- <div id="load_screen">
    <div id="loading"></div>
</div> -->





</body>
</html>